<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Storage;
use App\Models\Product;
use App\Models\ProductImage;

class HomeController extends Controller
{
    public function index()
    {
        $product = Product::with('images')->get();
        return view('product',compact('product'));
    }


    public function addProduct(Request $request)
    {
        
        $this->validate($request,[
            'name'=>'required',
            'sku'=>'required|unique:products',
            'short_desc'=>'required',
            'description' => 'required',
            'price' => 'required|integer',
        ]);

        $product = new Product;
        $product->name = $request->name;
        $product->sku = $request->sku;
        $product->short_desc = $request->short_desc;
        $product->description = $request->description;
        $product->description = $request->description;
        $product->price = $request->price;
        $product->status = $request->status;

        $product->save();

        foreach($request->file('images') as $imagefile)
        {
            $image = new ProductImage;
            $path = $imagefile->store('/images/product',['disk' => 'myfiles']);
            $image->url = $path;
            $image->product_id = $product->id;
            $image->save();
        }
        return redirect('/');

    }


    public function updateProduct($id)
    {
        $product = Product::with('images')->find($id);

        return view('update',compact('product'));

    }

    public function update(Request $request)
    {
        $this->validate($request,[
            'name'=>'required',
            'sku'=>'required',
            'short_desc'=>'required',
            'description' => 'required',
            'price' => 'required|integer',
        ]);

        $product = Product::find($request->id);
        $product->name = $request->name;
        $product->sku = $request->sku;
        $product->short_desc = $request->short_desc;
        $product->description = $request->description;
        $product->description = $request->description;
        $product->price = $request->price;
        $product->status = $request->status;

        $product->save();

        if($request->file('images'))
        {
            foreach($request->file('images') as $imagefile)
            {
                $image = new ProductImage;
                $path = $imagefile->store('/images/product',['disk' => 'myfiles']);
                $image->url = $path;
                $image->product_id = $product->id;
                $image->save();
            }
        }

        return redirect('/');
        
    }


    public function deleteProduct($id)
    {
     $product = Product::find($id);

     $product->delete();

     $product_image = ProductImage::where('product_id', $id)->get();
     foreach($product_image as $product)
     {
         $product->delete();
     }
     return redirect('/');
    }
}
