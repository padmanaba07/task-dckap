<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class,'index']);

Route::post('addProduct', [HomeController::class,'addProduct'])->name('products.uploadproducts');
Route::get('updateProduct/{id}', [HomeController::class,'updateProduct'])->name('products.updateproducts');
Route::get('deleteProduct/{id}', [HomeController::class,'deleteProduct'])->name('products.deleteProducts');
Route::post('update/{id}', [HomeController::class,'update'])->name('products.update');
