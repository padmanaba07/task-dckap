<!doctype html>

<html lang="{{ app()->getLocale() }}">

<head>

<meta charset="utf-8">

<meta http-equiv="X-UA-Compatible" content="IE=edge">

<meta name="viewport" content="width=device-width, initial-scale=1">

<title>Laravel Uploading</title>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

<!-- Optional theme -->

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

<!-- Fonts -->

<link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

<!-- Styles -->

<style>

.container {

margin-top:2%;

}

</style>

</head>

<body>

@if (count($errors) > 0)

<div class="alert alert-danger">

<ul>

@foreach ($errors->all() as $error)

<li>{{ $error }}</li>

@endforeach

</ul>

</div>

@endif

<div class="container">

<div class="row">


<div class="col-md-8"><h2>Product uploading</h2>

</div>

</div>

<br>

<div class="row">

<div class="col-md-3"></div>

<div class="col-md-6">

<form action="{{url('update/'.$product->id)}}" method="post" enctype="multipart/form-data">

{{ csrf_field() }}

<div class="form-group">

<input type="hidden" name="id" class="form-control" value="{{$product->id}}"  >

<label for="Product Name">Product Name</label>

<input type="text" name="name" onkeypress="return isCharacter(event)" class="form-control" value="{{$product->name}}"  placeholder="Product Name" >

<br />
<label for="SKU">SKU</label>

<input type="text" name="sku" onkeypress="return isCharacter(event)" class="form-control"  placeholder="SKU" value="{{$product->sku}}">


<br />
<label for="short_desc">Short Description</label>

<textarea name="short_desc" onkeypress="return isCharacter(event)" class="form-control" rows="4" placeholder="Description" > {{$product->description}}</textarea>
<br />


<label for="price">Price    </label>

<input type="number" name="price" class="form-control"  placeholder="price" value="{{$product->price}}" >
<br />



<label for="short_desc">Description</label>

<textarea name="description" onkeypress="return isCharacter(event)" class="form-control" rows="4" >{{$product->description}}</textarea>


</div>
@foreach($product->images as $images)
<img src ="{{url('/'.$images->url)}}" alt="No image" width="100" height="100" />
@endforeach

<label for="Product Name">Product photos (can attach more than one):</label>

<br />

<input type="file" class="form-control" name="images[]" multiple />

<br /><br />

<div class="form-check">
  <input type="radio" class="form-check-input" id="active" name="status" value="Active" {{$product->status == 'Active' ? 'checked' : ''}}>Active
  <label class="form-check-label" for="active"></label>
</div>
<div class="form-check">
  <input type="radio" class="form-check-input" id="inactive" name="status" value="Inactive" {{$product->status == 'Inactive' ? 'checked' : ''}}>Inactive
  <label class="form-check-label" for="inactive"></label>
</div>

<input type="submit" class="btn btn-primary" value="Upload" />

</form>

</div>

</div>

</div>
</body>

<script>
function isCharacter(evt)
  {
     var charCode = (evt.which) ? evt.which : event.keyCode
     if (charCode > 31 && (charCode < 48 || charCode > 57))
        return true;

     return false;
  }

</script>

</html>